#ifndef PORT_H
#define PORT_H

#include "systemc.h"
#include <stdio.h>
#include <set>
#include <map>
#include <string>
#include <memory>

#include "type_defs.h"

using namespace std;
class CMOD;

class Port
{
  public:
  string moduleName;
  string PortName;
  string longName;

  void splitModPortNames(string name);
  Port(string ln);
  string getType(CMOD& mod);

  bool operator<(const Port &other) const
  {
    return longName < other.longName;
  }
};




















#endif