#include "Port.h"
#include "Module.h"

using namespace std;

void Port::splitModPortNames(string name)
  {
    moduleName = name.substr(0, name.find("."));
    name.erase(0, name.find(".") + 1);
    PortName = name;
  }

Port::Port(string ln)
{
    splitModPortNames(ln);
    longName = ln;


}

string Port::getType(CMOD& mod){
    string type;
    if(this->moduleName == "THIS"){
      assert(mod.portType.find(PortName) != mod.portType.end());
      type = mod.portType[PortName];
    }
    else{
      CMOD* submod = mod.getSubMod(this->moduleName);
      assert(submod->portType.find(PortName) != submod->portType.end());
      type = submod->portType[PortName];
    }
    return type;
}