#ifndef STIM_CGRA_H
#define STIM_CGRA_H

#include "systemc.h"
#include "type_defs.h"

SC_MODULE(stim_cgra)
{
  sc_out<Word> CGRA_NORTH_I, CGRA_WEST_I, CGRA_EAST_I, CGRA_SOUTH_I;
  sc_in<bool> clk;

  void waitN(int n_cycles){
    for(int i=0; i<n_cycles; i++){
      wait();
    }
  }

  void StimGen()
  {
    // Word blank;
    // // wait();
    // CGRA_NORTH_I.write(blank);
    // CGRA_EAST_I.write(blank);
    // CGRA_NORTH_I.write(Word(1));
    // CGRA_EAST_I.write(Word(10));
    // wait();

    // CGRA_NORTH_I.write(blank);
    // CGRA_EAST_I.write(blank);
    // CGRA_NORTH_I.write(Word(100));
    // CGRA_EAST_I.write(Word(1000));
    // wait();

    // CGRA_NORTH_I.write(blank);
    // CGRA_EAST_I.write(blank);
    // CGRA_NORTH_I.write(Word(50));
    // wait();

    // CGRA_NORTH_I.write(blank);
    // CGRA_EAST_I.write(blank);
    // wait();
    
    // wait();
    // wait();
    // wait();
    // wait();
    // wait();
    // wait();

    waitN(6);
    sc_stop();
  }
  SC_CTOR(stim_cgra)
  {
    SC_THREAD(StimGen);
    sensitive << clk.pos();
  }
};



#endif
