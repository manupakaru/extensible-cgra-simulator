#ifndef MDP_H
#define MDP_H

#include "systemc.h"
#include <stdio.h>
#include <set>
#include <map>
#include <string>
#include <memory>
#include <sstream>
#include "Module.h"

// Needed for the simple_target_socket
#define SC_INCLUDE_DYNAMIC_PROCESSES

using namespace sc_core;
using namespace sc_dt;
using namespace std;

#include "tlm.h"
#include "tlm_utils/simple_initiator_socket.h"

#include <nlohmann/json.hpp>
using json = nlohmann::json;
using namespace std;

struct MDP : public CMOD 
{
protected:
public:
    void compute();
    bool parseConst(string s, int& val);

    sc_vector<tlm_utils::simple_initiator_socket<MDP>> is_sockets;
    tlm_utils::simple_initiator_socket<MDP> *get_socket(string name);



    map<string, int> isSocketNameMap;

    int size;
    int no_ports;


    MDP(sc_module_name name, map<string, json> &all_mods, json &hardware_template, string type, json &prog, json &parent_prog, int _ii) : CMOD(name, all_mods, hardware_template, type, prog, parent_prog, _ii)
    {
        // SPM(sc_module_name name, int size, int ports, string fileName) : sc_module(name), sockets("port"){
        // Register callback for incoming b_transport interface method call
        // SC_HAS_PROCESS(SPM);

        SC_HAS_PROCESS(MDP);
        SC_CTHREAD(compute, clk.pos());

        i_socketNameMap.clear();
        t_socketNameMap.clear();

        if (hardware_template.find("SOCKETS") != hardware_template.end())
        {
            cout << "SOCKETS size = " << hardware_template["SOCKETS"].size() << "\n";
            is_sockets.init(hardware_template["SOCKETS"].size());
            cout << "SOCKETS:";

            int count = 0;
            for (auto &el : hardware_template["SOCKETS"].items())
            {
                cout << el.value() << ",";
                isSocketNameMap[el.value()] = count;
                portType[el.value()] = "is_socket";
                count++;
            }
            cout << "\n";
        }
    }
};

#endif