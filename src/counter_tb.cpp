#include "systemc.h"
#include "counter.h"
#include "stim.h"
#include "stim_fu.h"
#include "stim_cgra.h"
#include "mon.h"
#include "CGRA.h"
#include "Module.h"
#include "initiator.h"

#include <cstdlib>
#include <cstdio>
#include <iostream>

#include <nlohmann/json.hpp>

// for convenience
using json = nlohmann::json;
map<string,json> all_mods;

void populate_all_mods(json archJSONFile){
  cout << "starting pop all mods\n";
  assert(!archJSONFile["ARCH"].is_null());

  for(auto& el : archJSONFile["ARCH"].items()){
    all_mods[el.key()]=archJSONFile["ARCH"][el.key()];
  }
  cout << "ending pop all mods\n";
}

void my_sc_trace(sc_trace_file *wf, sc_object* signal, string name){
  assert(signal);
  // cout <<
  if(sc_in<Word>* sin = dynamic_cast<sc_in<Word>*>(signal)){
    string module_name = sin->get_parent()->basename();
    string port_hier_name = module_name + "." + name;
    sc_trace(wf, *sin, port_hier_name);
  }
  else if(sc_out<Word>* sout = dynamic_cast<sc_out<Word>*>(signal)){
    string module_name = sout->get_parent()->basename();
    string port_hier_name = module_name + "." + name;
    sc_trace(wf, *sout, port_hier_name);
  }
  else if(sc_signal<Word>* sig = dynamic_cast<sc_signal<Word>*>(signal)){
    string module_name = sig->get_parent()->basename();
    string port_hier_name = module_name + "." + name;
    sc_trace(wf, *sig, port_hier_name);
  }
  else{
    assert(false);
  }
}


int sc_main (int argc, char* argv[]) {
  sc_clock clock("TestClock", 10, SC_NS,0.5, 1, SC_NS);
  sc_signal<bool>   reset;
  sc_signal<bool>   enable;
  sc_signal<sc_uint<4> > counter_out;
  int i = 0;
  // Connect the DUT
  first_counter counter("COUNTER");
    counter.clock(clock);
    counter.reset(reset);
    counter.enable(enable);
    counter.counter_out(counter_out);

  // Open VCD file
  sc_trace_file *wf = sc_create_vcd_trace_file("counter");
  // Dump the desired signals
  sc_trace(wf, clock, "clock");
  sc_trace(wf, reset, "reset");
  sc_trace(wf, enable, "enable");
  sc_trace(wf, counter_out, "count");

  stim stim_ins("stim");
  stim_ins.enable(enable);
	stim_ins.reset(reset);
	stim_ins.clk(clock);

  mon mon_ins("mon");
	mon_ins.clk(clock);
	mon_ins.counter(counter_out);

  //parse arch json file
  std::ifstream archFile("../json/cgra_stdnoc_mem.json"); assert(archFile);
  json archJSONFile; 
  archFile >> archJSONFile;

  //parse compiled program json file
  std::ifstream progFile("../json/cgra_config_test_mem.json"); assert(progFile);
  json progJSONFile;
  progFile >> progJSONFile;

  populate_all_mods(archJSONFile);

  sc_signal<Word> CGRA_NORTH_I, CGRA_EAST_I, CGRA_WEST_I, CGRA_SOUTH_I;

  CMOD cgra_ins("CGRA_INS",all_mods,archJSONFile["ARCH"]["CGRA"],"CGRA_INS",progJSONFile["CGRA_INS"],progJSONFile,progJSONFile["II"]);
  // return 0;

  cgra_ins.clk(clock);
  cgra_ins.reset(reset);
  cgra_ins.get_input("CGRA_NORTH_I")->bind(CGRA_NORTH_I);
  cgra_ins.get_input("CGRA_EAST_I")->bind(CGRA_EAST_I);
  cgra_ins.get_input("CGRA_WEST_I")->bind(CGRA_WEST_I);
  cgra_ins.get_input("CGRA_SOUTH_I")->bind(CGRA_SOUTH_I);

  stim_cgra stim_cgra_ins("stim_cgra");
  stim_cgra_ins.clk(clock);
  stim_cgra_ins.CGRA_NORTH_I(CGRA_NORTH_I);
  stim_cgra_ins.CGRA_EAST_I(CGRA_EAST_I);
  stim_cgra_ins.CGRA_WEST_I(CGRA_WEST_I);
  stim_cgra_ins.CGRA_SOUTH_I(CGRA_SOUTH_I);

  sc_trace(wf, CGRA_NORTH_I, "CGRA_NORTH_I");
  sc_trace(wf, CGRA_EAST_I, "CGRA_EAST_I");
  sc_trace(wf, CGRA_WEST_I, "CGRA_WEST_I");
  sc_trace(wf, CGRA_SOUTH_I, "CGRA_SOUTH_I");

  sc_trace(wf, *cgra_ins.getSubMod("PE_0_0")->get_input("NORTH_I"), "PE_0_0_NORTH_I");
  sc_trace(wf, *cgra_ins.getSubMod("PE_0_0")->getSubMod("FU0")->get_input("DP0_I1"), "PE_0_0.FU0.DP0_I1");
  sc_trace(wf, *cgra_ins.getSubMod("PE_0_0")->get_internal("FU0_DP0_I1"), "PE_0_0_FU0_DP0_I1");
  sc_trace(wf, *cgra_ins.getSubMod("PE_0_0")->getSubMod("FU0")->get_input("DP0_I2"), "PE_0_0.FU0.DP0_I2");
  sc_trace(wf, *cgra_ins.getSubMod("PE_0_0")->get_internal("FU0_DP0_I2"), "PE_0_0_FU0_DP0_I2");

  sc_trace(wf, *cgra_ins.getSubMod("PE_0_0")->getSubMod("FU0")->getSubMod("DP0")->get_internal("T_INT"), "PE_0_0.FU0.DP0.T_INT");
  sc_trace(wf, *cgra_ins.getSubMod("PE_0_0")->getSubMod("FU0")->get_output("DP0_T"), "PE_0_0.FU0.DP0_T");

  sc_trace(wf, cgra_ins.getSubMod("PE_0_0")->current_cycle, "PE_0_0.current_cycle");


  my_sc_trace(wf,cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->get_input("WP0"),"WP0");
  my_sc_trace(wf,cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->get_input("WP1"),"WP1");
  my_sc_trace(wf,cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->get_output("RP0"),"RP0");
  my_sc_trace(wf,cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->get_output("RP1"),"RP1");

  my_sc_trace(wf,cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->get_reg("R0"),"R0");
  my_sc_trace(wf,cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->get_reg("R1"),"R1");
  my_sc_trace(wf,cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->get_reg("R2"),"R2");
  my_sc_trace(wf,cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->get_reg("R3"),"R3");

  // sc_trace(wf, *cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->get_reg("R3"), "R3");
  
  // sc_trace(wf, *cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->get_input("WP0"), string(cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->basename) + std::string(".WP0"));
  // sc_trace(wf, *cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->get_input("WP1"), string(cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->basename) + string(".WP1"));
  // sc_trace(wf, *cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->get_input("RP0"), string(cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->basename) + ".RP0");
  // sc_trace(wf, *cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->get_input("RP1"), string(cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->basename) + ".RP1");

  // sc_trace(wf, *cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->get_reg("R0"), string(cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->basename) + ".R0");
  // sc_trace(wf, *cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->get_reg("R1"), string(cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->basename) + ".R1");
  // sc_trace(wf, *cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->get_reg("R2"), string(cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->basename) + ".R2");
  // sc_trace(wf, *cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->get_reg("R3"), string(cgra_ins.getSubMod("PE_0_0")->getSubMod("RF0")->basename) + ".R3");



  sc_trace(wf, *cgra_ins.get_output("CGRA_SOUTH_O"), "CGRA_SOUTH_O");

  // fu_ins.clk(clock);
  // fu_ins.get_input("DP0_I1")->bind(fu_I1);
  // fu_ins.get_input("DP0_I2")->bind(fu_I2);
  // fu_ins.get_input("DP0_P")->bind(fu_P);
  // // (*fu_ins.get_output("DP0_T"))(fu_T);

  // stim_fu stim_fu_ins("stim_fu");
  // stim_fu_ins.clk(clock);
  // stim_fu_ins.fu_I1(fu_I1);
  // stim_fu_ins.fu_I2(fu_I2);

  // sc_trace(wf, fu_I1, "fu_I1");
  // sc_trace(wf, fu_I2, "fu_I2");
  // sc_trace(wf, (*fu_ins.get_output("DP0_T")), "fu_T");
  // sc_trace(wf, (*fu_ins.getSubMod("DP0")->get_output("T")), "DP0_T");

  sc_start();

  cout << "@" << sc_time_stamp() <<" Terminating simulation\n" << endl;
  sc_close_vcd_trace_file(wf);
  return 0;// Terminate simulation

 }
