#include "systemc.h"
#include <stdio.h>

using namespace std;

SC_MODULE(CGRA)
{

  public:
  sc_in<bool> clk;

  void StimGen()
  {
    cout << "time=" << sc_time_stamp() << ",";  
  }
  SC_CTOR(CGRA)
  {
    SC_METHOD(StimGen);
    sensitive << clk.pos();
  }
  void parseJSON(ifstream& fileIn);

  private:


};
