#include "DP.h"


bool DP::parseConst(string s, int& val){
    string keyword = s.substr(0, s.find("."));
    s.erase(0, s.find(".") + 1);
    string value = s;

    if(keyword == "CONST"){
        stringstream iss(value);
        iss >> val;
        return true;
    }
    return false;
}

void DP::compute(){

    while (true)
    {
        bool I1_expected = false;
        bool I2_expected = false;
        bool P_expected = false;

        pair<int,bool> I1_const;
        pair<int,bool> I2_const;

        Word I1_data = get_input("I1")->read();
        Word I2_data = get_input("I2")->read();
        Word P_data = get_input("P")->read();

        assert(parent_prog["TYPE"] == "FU");
        for (auto &el : parent_prog["CONNECTIONS"][to_string(current_cycle)].items())
        {
            Port dstPort(el.value());
            string src = el.key();
            if (dstPort.PortName == "I1")
            {
                int data;
                if(parseConst(src,data)) I1_data = Word(data);
                I1_expected = true;
            }
            else if (dstPort.PortName == "I2")
            {
                int data;
                if(parseConst(src,data)) I2_data = Word(data);
                I2_expected = true;
            }
            else if (dstPort.PortName == "P")
            {
                P_expected = true;
            }
        }



        bool do_compute = true;
        if (I1_expected && !I1_data.valid)
        {
            cout << "I1 data expected and not valid\n";
            do_compute = false;
        }
        if (I2_expected && !I2_data.valid)
        {
            cout << "I2 data expected and not valid\n";
            do_compute = false;
        }

        if (P_expected && !P_data.valid)
        {
            cout << "P data expected and not valid\n";
            do_compute = false;
        }
        else if (P_expected && P_data.valid && (P_data.data == 0))
        {
            cout << "P data expected, valid but predicates is false\n";
            do_compute = false;
        }

        // assert(false);

        //Resolve OP code
        if (parent_prog["OPS"].find(to_string(current_cycle)) != parent_prog["OPS"].end() && do_compute)
        {
            if (parent_prog["OPS"][to_string(current_cycle)] == "ADD")
            {
                cout << "ADD :: ";
                cout << "compute :: I1=" << I1_data.safe_read() << ",I2=" << I2_data.safe_read() << "\n";
                get_internal("T_INT")->write(Word(I1_data.safe_read() + I2_data.safe_read()));
            }
            else if (parent_prog["OPS"][to_string(current_cycle)] == "SUB")
            {
                cout << "SUB :: ";
                cout << "compute :: I1=" << I1_data.safe_read() << ",I2=" << I2_data.safe_read() << "\n";
                get_internal("T_INT")->write(Word(I1_data.safe_read() - I2_data.safe_read()));
            }
            else if (parent_prog["OPS"][to_string(current_cycle)] == "MUL")
            {
                cout << "MUL :: ";
                cout << "compute :: I1=" << I1_data.safe_read() << ",I2=" << I2_data.safe_read() << "\n";
                get_internal("T_INT")->write(Word(I1_data.safe_read() * I2_data.safe_read()));
            }
            else
            {
                cout << "OP : " << parent_prog["OPS"][to_string(current_cycle)] << "is not implemented. Exiting...\n";
                assert(false);
            }
        }
        else
        {
            Word tempW(0);
            tempW.valid = 0;
            get_internal("T_INT")->write(tempW);
        }

        wait();
    }
}