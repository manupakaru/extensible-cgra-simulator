#include "MDP.h"


tlm_utils::simple_initiator_socket<MDP>* MDP::get_socket(string name){
  assert(isSocketNameMap.find(name) != isSocketNameMap.end());
  tlm_utils::simple_initiator_socket<MDP>* retVal = &is_sockets[isSocketNameMap[name]];
  assert(retVal);
  return retVal;
    // return &data_socket;
}

bool MDP::parseConst(string s, int& val){
    string keyword = s.substr(0, s.find("."));
    s.erase(0, s.find(".") + 1);
    string value = s;

    if(keyword == "CONST"){
        stringstream iss(value);
        iss >> val;
        return true;
    }
    return false;
}


void MDP::compute(){

    while (true)
    {
        bool I1_expected = false;
        bool I2_expected = false;
        bool P_expected = false;


        Word I1_data = get_input("I1")->read();
        Word I2_data = get_input("I2")->read();
        Word P_data = get_input("P")->read();

        assert(parent_prog["TYPE"] == "FU_MEM");
       for (auto &el : parent_prog["CONNECTIONS"][to_string(current_cycle)].items())
        {
            Port dstPort(el.value());
            string src = el.key();
            if (dstPort.PortName == "I1")
            {
                int data;
                if(parseConst(src,data)) I1_data = Word(data);
                I1_expected = true;
            }
            else if (dstPort.PortName == "I2")
            {
                int data;
                if(parseConst(src,data)) I2_data = Word(data);
                I2_expected = true;
            }
            else if (dstPort.PortName == "P")
            {
                P_expected = true;
            }
        }


        bool do_compute = true;
        if (I1_expected && !I1_data.valid)
        {
            cout << "I1 data expected and not valid\n";
            do_compute = false;
        }
        if (I2_expected && !I2_data.valid)
        {
            cout << "I2 data expected and not valid\n";
            do_compute = false;
        }

        if (P_expected && !P_data.valid)
        {
            cout << "P data expected and not valid\n";
            do_compute = false;
        }
        else if (P_expected && P_data.valid && (P_data.data == 0))
        {
            cout << "P data expected, valid but predicates is false\n";
            do_compute = false;
        }

        // assert(false);

        //Resolve OP code
        if (parent_prog["OPS"].find(to_string(current_cycle)) != parent_prog["OPS"].end() && do_compute)
        {

            string opcode = parent_prog["OPS"][to_string(current_cycle)];

            if (opcode == "ADD")
            {
                cout << "ADD :: ";
                cout << "compute :: I1=" << I1_data.safe_read() << ",I2=" << I2_data.safe_read() << "\n";
                get_internal("T_INT")->write(Word(I1_data.safe_read() + I2_data.safe_read()));
            }
            else if (opcode == "SUB")
            {
                cout << "SUB :: ";
                cout << "compute :: I1=" << I1_data.safe_read() << ",I2=" << I2_data.safe_read() << "\n";
                get_internal("T_INT")->write(Word(I1_data.safe_read() - I2_data.safe_read()));
            }
            else if (opcode == "MUL")
            {
                cout << "MUL :: ";
                cout << "compute :: I1=" << I1_data.safe_read() << ",I2=" << I2_data.safe_read() << "\n";
                get_internal("T_INT")->write(Word(I1_data.safe_read() * I2_data.safe_read()));
            }
            else if(opcode.find("LOAD") != string::npos)
            {
                cout << "LOAD :: ";
                cout << "compute :: I1=" << I1_data.safe_read() << ",I2(addr)=" << I2_data.safe_read() << "\n";

                int32_t data = 0;

                tlm::tlm_generic_payload* t = new tlm::tlm_generic_payload;
                t->set_command(tlm::TLM_READ_COMMAND);
                t->set_address(I2_data.safe_read());
                t->set_data_ptr(reinterpret_cast<unsigned char*>(&data));
        
                int data_length = -1;
                if(opcode == "LOAD"){
                    data_length = 4;
                }
                else if(opcode == "LOADH"){
                    data_length = 2;
                }
                else{
                    assert(opcode == "LOADB");
                    data_length = 1;
                }
                
                t->set_data_length(data_length);
                t->set_streaming_width(data_length); // = data_length, no streaming
                t->set_byte_enable_ptr(0); //unused
                t->set_dmi_allowed( false );
                t->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);

                assert(isSocketNameMap.size() == 1);
                string socket_name = (*isSocketNameMap.begin()).first;

                sc_time delay = sc_time(SC_ZERO_TIME);
                (*get_socket(socket_name))->b_transport(*t,delay);


                if(t->is_response_error()){
                    SC_REPORT_ERROR("TLM-2", "Response error from b_transport");
                }

                cout << "LOAD DONE.!, data=" << data << "\n";
                get_internal("T_INT")->write(Word(data));
            }
            else if(opcode.find("STORE") != string::npos)
            {
                cout << "STORE :: ";
                cout << "compute :: I1(data)=" << I1_data.safe_read() << ",I2(addr)=" << I2_data.safe_read() << "\n";

                int32_t data = I1_data.safe_read();

                tlm::tlm_generic_payload* t = new tlm::tlm_generic_payload;
                t->set_command(tlm::TLM_WRITE_COMMAND);
                t->set_address(I2_data.safe_read());
                t->set_data_ptr(reinterpret_cast<unsigned char*>(&data));
        
                int data_length = -1;
                if(opcode == "STORE"){
                    data_length = 4;
                }
                else if(opcode == "STOREH"){
                    data_length = 2;
                }
                else{
                    assert(opcode == "STOREB");
                    data_length = 1;
                }
                
                t->set_data_length(data_length);
                t->set_streaming_width(data_length); // = data_length, no streaming
                t->set_byte_enable_ptr(0); //unused
                t->set_dmi_allowed( false );
                t->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);

                assert(isSocketNameMap.size() == 1);
                string socket_name = (*isSocketNameMap.begin()).first;

                sc_time delay = sc_time(SC_ZERO_TIME);
                (*get_socket(socket_name))->b_transport(*t,delay);


                if(t->is_response_error()){
                    SC_REPORT_ERROR("TLM-2", "Response error from b_transport");
                }

                get_internal("T_INT")->write(Word(data));
            }
            else
            {
                cout << "OP : " << parent_prog["OPS"][to_string(current_cycle)] << "is not implemented. Exiting...\n";
                assert(false);
            }
        }
        else
        {
            Word tempW(0);
            tempW.valid = 0;
            get_internal("T_INT")->write(tempW);
        }

        wait();
    }


}