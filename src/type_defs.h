#ifndef TYPE_DEFS_H
#define TYPE_DEFS_H

#include <systemc.h>

struct Word{
    sc_int<32> data;
    bool valid;

    Word(int32_t _data){
        this->data = _data;
        this->valid = true;
    }
    Word(){
        this->data = 0;
        this->valid = false;
    }
    void write(int32_t _data){
        this->data = _data;
        this->valid = true;
    }
    int32_t safe_read(){
        return this->data;
    }
    int32_t destruct_read(){
        this->valid = false;
        return this->data;
    }
    Word getCopy(){
        Word temp(data);
        return temp;
    }
    bool operator==(const Word& other) const{
        return (this->data == other.data) && (this->valid == other.valid);
    }
    bool operator!=(const Word& other) const{
        return !(this->data == other.data) && (this->valid == other.valid);
    }
    friend std::ostream& operator << (std::ostream& o, const Word& w){
        o << "v:" << w.valid << "|d:" << w.data;
    }
};

//Overload function
inline void sc_trace( sc_trace_file* _f, const Word& _w, const std::string& _s ) {
   sc_trace( _f, _w.valid, _s + "_v" );
   sc_trace( _f, _w.data, _s + "_d" );
}

// typedef int32_t Word;










#endif