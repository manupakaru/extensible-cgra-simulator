#ifndef MODULE_H
#define MODULE_H


#include "systemc.h"
#include <stdio.h>
#include <set>
#include <map>
#include <string>
#include <memory>

#include "type_defs.h"

using namespace std;

#include <nlohmann/json.hpp>
#include "Port.h"

#include "tlm.h"
#include "tlm_utils/simple_target_socket.h"

// for convenience
using json = nlohmann::json;


SC_MODULE(CMOD)
{

protected:
  sc_vector<sc_in<Word>> inputs;
  sc_vector<sc_out<Word>> outputs;
  sc_vector<sc_signal<Word>> internals;
  sc_vector<sc_signal<Word>> regs;

  // sc_vector<tlm::tlm_initiator_socket<32>> sockets;
  sc_vector<tlm::tlm_initiator_socket<32>> i_sockets;
  sc_vector<tlm::tlm_target_socket<32>> t_sockets;



  vector<unique_ptr<CMOD>> submods;
  map<Port, set<Port>> possibleMuxes;
  map<Port, set<Port>> connectToPorts;
  set<Port> leafs;



public:
  sc_in<bool> clk{"clk"};
  sc_in<bool> reset{"reset"};
  // std::set<sc_in<Word>> inputs;
  // std::set<sc_out<Word>> outputs;
  // std::set<sc_signal<Word>> internals;
  // std::set<CMOD*> submods;

  sc_in<Word> *get_input(string name);
  sc_out<Word> *get_output(string name);
  sc_signal<Word> *get_internal(string name);
  sc_signal<Word> *get_reg(string name);

  tlm::tlm_initiator_socket<32> *get_i_socket(string name);
  tlm::tlm_target_socket<32> *get_t_socket(string name);

  CMOD *getSubMod(string name);

  void assignPort(Port p, Word w);
  Word readPort(Port p);

  string getSigType(string name);
  

  string type;
  string subname;

  int ii;
  sc_signal<int> current_cycle;
  json current_prog;
  json parent_prog;

  map<string, int> inputNameMap, outputNameMap, intNameMap, regNameMap, t_socketNameMap, i_socketNameMap;
  map<string, string> portType;

  void StimGen()
  {
    cout << "time=" << sc_time_stamp() << ","
         << "module_name = " << this->name() << "\n";
    route();
  }

  void fillPorts(json & hardware_template);
  void printPorts();
  void fillMuxes(json & hardware_template);

  void route();
  void incr_cycle_counter();

  void bindNonMuxConnections();

  CMOD(sc_module_name name, map<string, json> & all_mods, json & hardware_template, string type, json & prog, json& parent_prog, int _ii);
  void parseJSON(ifstream & fileIn);

private:
};


#endif