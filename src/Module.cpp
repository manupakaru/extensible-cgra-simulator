#include "Module.h"
#include "SPM.h"
#include "MDP.h"
#include "DP.h"
#include <nlohmann/json.hpp>
#include <stack>

// for convenience
using json = nlohmann::json;
using namespace std;

void CMOD::fillPorts(json &hardware_template)
{
    if (hardware_template.find("INPUTS") != hardware_template.end())
    {
        cout << "INPUTS size = " << hardware_template["INPUTS"].size() << "\n";
        inputs.init(hardware_template["INPUTS"].size());
        cout << "INPUTS:";

        int count = 0;
        for (auto &el : hardware_template["INPUTS"].items())
        {
            cout << el.value() << ",";
            inputNameMap[el.value()] = count;
            portType[el.value()] = "input";
            string name = el.value();
            count++;
        }
        cout << "\n";
    }

    if (hardware_template.find("OUTPUTS") != hardware_template.end())
    {
        cout << "OUTPUT size = " << hardware_template["OUTPUTS"].size() << "\n";
        outputs.init(hardware_template["OUTPUTS"].size());
        cout << "OUTPUTS:";

        int count = 0;
        for (auto &el : hardware_template["OUTPUTS"].items())
        {
            cout << el.value() << ",";
            outputNameMap[el.value()] = count;
            portType[el.value()] = "output";
            count++;
        }
        cout << "\n";
    }

    if (hardware_template.find("INTERNALS") != hardware_template.end())
    {
        cout << "INTERNALS size = " << hardware_template["INTERNALS"].size() << "\n";
        internals.init(hardware_template["INTERNALS"].size());
        cout << "INTERNALS:";

        int count = 0;
        for (auto &el : hardware_template["INTERNALS"].items())
        {
            cout << el.value() << ",";
            intNameMap[el.value()] = count;
            portType[el.value()] = "int";
            count++;
        }
        cout << "\n";
    }

    if (hardware_template.find("REGS") != hardware_template.end())
    {
        cout << "REGS size = " << hardware_template["REGS"].size() << "\n";
        regs.init(hardware_template["REGS"].size());
        cout << "REGS:";

        int count = 0;
        for (auto &el : hardware_template["REGS"].items())
        {
            cout << el.value() << ",";
            regNameMap[el.value()] = count;
            portType[el.value()] = "reg";
            count++;
        }
        cout << "\n";
    }

    if (hardware_template.find("TSOCKETS") != hardware_template.end())
    {
        cout << "TSOCKETS size = " << hardware_template["TSOCKETS"].size() << "\n";
        t_sockets.init(hardware_template["TSOCKETS"].size());
        cout << "TSOCKETS:";

        int count = 0;
        for (auto &el : hardware_template["TSOCKETS"].items())
        {
            cout << el.value() << ",";
            t_socketNameMap[el.value()] = count;
            portType[el.value()] = "t_socket";
            count++;
        }
        cout << "\n";
    }

    if (hardware_template.find("ISOCKETS") != hardware_template.end())
    {
        cout << "ISOCKETS size = " << hardware_template["ISOCKETS"].size() << "\n";
        i_sockets.init(hardware_template["ISOCKETS"].size());
        cout << "ISOCKETS:";

        int count = 0;
        for (auto &el : hardware_template["ISOCKETS"].items())
        {
            cout << el.value() << ",";
            i_socketNameMap[el.value()] = count;
            portType[el.value()] = "i_socket";
            count++;
        }
        cout << "\n";
    }
}

void CMOD::fillMuxes(json &hardware_template)
{
    if (hardware_template.find("CONNECTIONS") != hardware_template.end())
    {
        for (auto &el : hardware_template["CONNECTIONS"].items())
        {
            cout << "fromPort=" << el.key() << ",toPorts=";
            leafs.insert(Port(el.key()));
            for (auto &p : el.value())
            {
                cout << p << ",";
                possibleMuxes[Port(p)].insert(Port(el.key()));
                connectToPorts[Port(el.key())].insert(Port(p));
            }
            cout << "\n";
        }
    }

    for (pair<Port, set<Port>> pair : possibleMuxes)
    {
        if (getSigType(pair.first.PortName) != "reg")
        {
            leafs.erase(pair.first);
        }
    }

    cout << "leafs :: ";
    for (Port l : leafs)
    {
        cout << l.longName << ",";
    }
    cout << "\n";
}

void CMOD::printPorts()
{
    cout << "inputs:";
    // for(auto& ip : inputs){
    //     cout << ip.name() << ",";
    // }
    for (std::pair<string, int> p : inputNameMap)
    {
        string name = p.first;
        int idx = p.second;
        cout << name << "(" << inputs[idx].name() << "),";
    }
    cout << "\n";

    cout << "outputs:";
    for (auto &op : outputs)
    {
        cout << op.name() << ",";
    }
    cout << "\n";

    cout << "internals:";
    for (auto &intp : internals)
    {
        cout << intp.name() << ",";
    }
    cout << "\n";
}

sc_in<Word> *CMOD::get_input(string name)
{
    assert(inputNameMap.find(name) != inputNameMap.end());
    sc_in<Word> *retVal = &inputs[inputNameMap[name]];
    assert(retVal);
    return retVal;
}

sc_out<Word> *CMOD::get_output(string name)
{
    assert(outputNameMap.find(name) != outputNameMap.end());
    sc_out<Word> *retVal = &outputs[outputNameMap[name]];
    assert(retVal);
    return retVal;
}

sc_signal<Word> *CMOD::get_internal(string name)
{
    assert(intNameMap.find(name) != intNameMap.end());
    sc_signal<Word> *retVal = &internals[intNameMap[name]];
    assert(retVal);
    return retVal;
}

sc_signal<Word> *CMOD::get_reg(string name)
{
    assert(regNameMap.find(name) != regNameMap.end());
    sc_signal<Word> *retVal = &regs[regNameMap[name]];
    assert(retVal);
    return retVal;
}

tlm::tlm_initiator_socket<32> *CMOD::get_i_socket(string name)
{
    assert(i_socketNameMap.find(name) != i_socketNameMap.end());
    tlm::tlm_initiator_socket<32> *retVal = &i_sockets[i_socketNameMap[name]];
    assert(retVal);
    return retVal;
}

tlm::tlm_target_socket<32> *CMOD::get_t_socket(string name)
{
    assert(t_socketNameMap.find(name) != t_socketNameMap.end());
    tlm::tlm_target_socket<32> *retVal = &t_sockets[t_socketNameMap[name]];
    assert(retVal);
    return retVal;
}

string CMOD::getSigType(string name)
{
    if (inputNameMap.find(name) != inputNameMap.end())
    {
        return "input";
    }
    else if (intNameMap.find(name) != intNameMap.end())
    {
        return "int";
    }
    else if (outputNameMap.find(name) != outputNameMap.end())
    {
        return "output";
    }
    else if (regNameMap.find(name) != regNameMap.end())
    {
        return "reg";
    }
    else if (i_socketNameMap.find(name) != i_socketNameMap.end())
    {
        return "i_socket";
    }
    else if (t_socketNameMap.find(name) != t_socketNameMap.end())
    {
        return "t_socket";
    }
    else
    {
        return "none";
    }
}

void CMOD::bindNonMuxConnections()
{
    for (const pair<Port, set<Port>> pair : possibleMuxes)
    {
        if (pair.second.size() != 1)
            continue;
        Port dest_p = pair.first;
        Port src_p = *pair.second.begin();

        cout << "Connecting intra-module non-mux connections :: src=" << src_p.longName << ",dest=" << dest_p.longName << "\n";
        if (!(dest_p.moduleName == "THIS" && src_p.moduleName == "THIS"))
        {
            cout << "not intra-module! \n";
            continue;
        }

        string srcportType = src_p.getType(*this);
        string destporttype = dest_p.getType(*this);

        if (srcportType == "input")
        {

            if (destporttype == "int")
            {
                // get_internal(dest_p.PortName)->bind(*get_input(src_p.PortName)); cout << "bound\n";
            }
            else if (destporttype == "output")
            {
                // get_output(dest_p.PortName)->bind(*get_input(src_p.PortName)); cout << "bound\n";
            }
            else
            {
                assert(false);
            }
        }
        else if (srcportType == "int")
        {
            if (destporttype == "int")
            {
                // get_internal(dest_p.PortName)->bind(*get_internal(src_p.PortName)); cout << "bound\n";
            }
            else if (destporttype == "output")
            {
                get_output(dest_p.PortName)->bind(*get_internal(src_p.PortName));
                cout << "bound\n";
            }
            else
            {
                assert(false);
            }
        }
        else
        {
            assert(false);
        }
    }
}


CMOD *CMOD::getSubMod(string name)
{
    for (const unique_ptr<CMOD> &sm : submods)
    {
        CMOD *cmod_ptr = sm.get();
        cout << "cmod_ptr->subname = " << cmod_ptr->subname << "\n";
        if (cmod_ptr->subname == name)
        {
            return cmod_ptr;
        }
    }
    cout << "Searching for submodule : " << name << " in the module : " << this->subname << "\n";
    assert(false);
}

void CMOD::route()
{

    stack<Port> updatedPorts;
    map<Port, set<Port>> activePorts;
    set<Port> usedPorts;

    for (auto &ap : current_prog["CONNECTIONS"][to_string(current_cycle)].items())
    {
        Port srcPort(ap.key());
        Port dstPort(ap.value());

        cout << "route in cycle=" << current_cycle << ",";
        cout << srcPort.longName << " to ";
        cout << dstPort.longName << "\n";
        activePorts[srcPort].insert(dstPort);

        usedPorts.insert(srcPort);
        usedPorts.insert(dstPort);
    }

    stack<Port> invalidatedPorts;

    for (Port sp : leafs)
    {
        if (activePorts.find(sp) != activePorts.end())
        {
            updatedPorts.push(sp);
        }
        else
        {
            invalidatedPorts.push(sp);
        }
    }

    while (!updatedPorts.empty())
    {
        Port topPort = updatedPorts.top();
        updatedPorts.pop();
        Word readWord = readPort(topPort);
        if (!activePorts[topPort].empty())
        {
            for (Port nextPort : activePorts[topPort])
            {
                if (nextPort.getType(*this) != "input" && nextPort.getType(*this) != "output")
                {
                    assignPort(nextPort, readWord);
                }
                updatedPorts.push(nextPort);
            }
        }
    }

    while (!invalidatedPorts.empty())
    {
        Port topPort = invalidatedPorts.top();
        invalidatedPorts.pop();
        if (!connectToPorts[topPort].empty())
        {
            for (Port nextPort : connectToPorts[topPort])
            {
                if (usedPorts.find(nextPort) == usedPorts.end())
                {
                    if (nextPort.getType(*this) != "input" 
                        && nextPort.getType(*this) != "output" 
                        && nextPort.getType(*this) != "reg"
                        && nextPort.getType(*this).find("socket") == string::npos) //it should not be socket type port
                    {
                        Word falseData;
                        assignPort(nextPort, falseData);
                        invalidatedPorts.push(nextPort);
                    }
                }
            }
        }
    }
}

void CMOD::incr_cycle_counter()
{
    //This is to be a clocked thread
    while (true)
    {
        // if(reset == 1){
        //     current_cycle = 0;
        // }
        // else{
        if (current_cycle + 1 == this->ii)
        {
            current_cycle = 0;
        }
        else
        {
            current_cycle = current_cycle + 1;
        }
        // }
        wait();
    }
}

void CMOD::assignPort(Port p, Word w)
{

    cout << "Assigning to port :: " << p.longName << "of module=" << this->subname << ", in cycle=" << current_cycle << ",value=" << w.data << "\n";

    if (p.getType(*this) == "socket")
    {
        cout << "Should not use assign port with TLM sockets.\n";
        assert(false);
    }

    if (p.getType(*this) == "input")
    {
        if (p.moduleName == "THIS")
        {
            cout << "should not write to a output of a sub-module!! exiting ...\n";
            assert(false);
        }
        else
        {
            cout << "Should not assign values for input ports directly,";
            cout << "rather json should have a singular internal port of the parent connected to the input of the submodule\n";
            assert(false);
        }
        cout << "should not assign values for input ports!! exiting....\n";
        assert(false);
    }
    else if (p.getType(*this) == "output")
    {
        if (p.moduleName == "THIS")
        {
            get_output(p.PortName)->write(w);
        }
        else
        {
            cout << "should not write to a output of a sub-module!! exiting ...\n";
            assert(false);
        }
    }
    else if (p.getType(*this) == "int")
    {
        if (p.moduleName == "THIS")
        {
            get_internal(p.PortName)->write(w);
        }
        else
        {
            cout << "should not write to a internal of a sub-module!! exiting ...\n";
            assert(false);
        }
    }
    else if (p.getType(*this) == "reg")
    {
        if (p.moduleName == "THIS")
        {
            get_reg(p.PortName)->write(w);
        }
        else
        {
            cout << "should not write to a reg of a sub-module!! exiting ...\n";
            assert(false);
        }
    }
    else
    {
        assert(false);
    }
}

Word CMOD::readPort(Port p)
{
    cout << "module name :: " << this->subname << "\n";
    cout << "reading port :: " << p.longName << "\n";

    if (p.getType(*this) == "socket")
    {
        cout << "Should not use socket with readPort.\n";
        assert(false);
    }

    if (p.getType(*this) == "input")
    {
        if (p.moduleName == "THIS")
        {
            return get_input(p.PortName)->read();
        }
        else
        {
            return getSubMod(p.moduleName)->get_input(p.PortName)->read();
        }
    }
    else if (p.getType(*this) == "output")
    {
        if (p.moduleName == "THIS")
        {
            return get_output(p.PortName)->read();
        }
        else
        {
            return getSubMod(p.moduleName)->get_output(p.PortName)->read();
        }
    }
    else if (p.getType(*this) == "int")
    {
        if (p.moduleName == "THIS")
        {
            Word retVal = get_internal(p.PortName)->read();
            Word retVal_copy = retVal.getCopy();
            retVal.destruct_read();
            assert(retVal.valid == false);
            // get_internal(p.PortName)->write(retVal);
            return retVal_copy;
        }
        else
        {
            // return getSubMod(p.moduleName)->get_internal(p.PortName)->read();
            Word retVal = getSubMod(p.moduleName)->get_internal(p.PortName)->read();
            Word retVal_copy = retVal.getCopy();
            retVal.destruct_read();
            assert(retVal.valid == false);
            // getSubMod(p.moduleName)->get_internal(p.PortName)->write(retVal);
            return retVal_copy;
        }
    }
    else if (p.getType(*this) == "reg")
    {
        if (p.moduleName == "THIS")
        {
            return get_reg(p.PortName)->read();
        }
        else
        {
            return getSubMod(p.moduleName)->get_reg(p.PortName)->read();
        }
    }
    else
    {
        assert(false);
    }
}

CMOD::CMOD(sc_module_name name, map<string, json> &all_mods, json &hardware_template, string type, json &prog, json &parent_prog, int _ii) : sc_module(name)
{
    SC_HAS_PROCESS(CMOD);
    SC_CTHREAD(incr_cycle_counter, clk.pos());

    this->type = type;
    this->ii = _ii;
    this->current_prog = prog;
    this->parent_prog = parent_prog;

    subname = name;

    fillPorts(hardware_template);
    printPorts();
    fillMuxes(hardware_template);

    bindNonMuxConnections();

    if (hardware_template.find("SUBMODS") != hardware_template.end())
    {
        for (auto &el : hardware_template["SUBMODS"].items())
        {

            assert(all_mods.find(el.key()) != all_mods.end());
            string mod_ins_name = el.value();
            string mod_type = el.key();
            json mod_info = all_mods[mod_type];

            cout << "mod_ins_name = " << mod_ins_name << "\n";
            if (prog["SUBMODS"].find(mod_ins_name) == prog["SUBMODS"].end())
            {
                cout << "Passing Empty Program for the sub-module : " << mod_ins_name << "\n";
            }
            json prog_info = prog["SUBMODS"][mod_ins_name];

            unique_ptr<CMOD> submod_up;

            if (mod_type == "SPM")
            {
                submod_up = unique_ptr<CMOD>((new SPM(mod_ins_name.c_str(), all_mods, mod_info, mod_type, prog_info, prog, _ii)));
            }
            else if (mod_type == "MDP")
            {
                submod_up = unique_ptr<CMOD>((new MDP(mod_ins_name.c_str(), all_mods, mod_info, mod_type, prog_info, prog, _ii)));
            }
            else if (mod_type == "DP"){
                submod_up = unique_ptr<CMOD>((new DP(mod_ins_name.c_str(), all_mods, mod_info, mod_type, prog_info, prog, _ii)));
            }
            else
            {
                submod_up = unique_ptr<CMOD>((new CMOD(mod_ins_name.c_str(), all_mods, mod_info, mod_type, prog_info, prog, _ii)));
            }

            submods.push_back(move(submod_up));
        }

        for (const std::unique_ptr<CMOD> &submod_ptr : submods)
        {

            CMOD *submod = submod_ptr.get();
            string mod_ins_name = submod->subname;
            string mod_type = submod->type;

            //add connections between parent and submodules
            submod->clk(clk);
            submod->reset(reset);
            for (const pair<Port, set<Port>> pair : possibleMuxes)
            {
                Port dest_p = pair.first;
                Port src_p = *pair.second.begin();
                bool bounded = false;
                string portType = "none";

                cout << "check 1\n";
                cout << "mod_ins_name = " << mod_ins_name << "\n";

                if (src_p.moduleName != "THIS" && dest_p.moduleName != "THIS")
                {
                    //do nothing
                }
                else if (dest_p.moduleName == mod_ins_name)
                {
                    assert(pair.second.size() == 1);

                    // portType = getSigType(src_p.PortName);
                    // assert(portType == src_p.getType(*this));
                    portType = src_p.getType(*this);
                    string destPortType = dest_p.getType(*this);

                    cout << "portType = " << portType << "\n";
                    cout << "srcportName = " << src_p.PortName << "\n";
                    cout << "destportName = " << dest_p.PortName << "\n";
                    if (portType == "input")
                    {
                        cout << "sys_c_port_name = " << submod->get_input(dest_p.PortName)->name() << "\n";
                        submod->get_input(dest_p.PortName)->bind(*get_input(src_p.PortName));
                    }
                    else if (portType == "int")
                    {
                        submod->get_input(dest_p.PortName)->bind(*get_internal(src_p.PortName));
                    }
                    else if (portType == "t_socket" || portType == "i_socket")
                    {
                        //if src port is a target/initator of the current module, then there can be 
                        // it should not be connected submodules
                        assert(false);
                    }
                    else
                    {
                        assert(false);
                    }
                    bounded = true;
                    cout << "check 2\n";
                }
                else if (src_p.moduleName == mod_ins_name && pair.second.size() == 1)
                {
                    assert(pair.second.size() == 1);

                    // portType = getSigType(dest_p.PortName);
                    // assert(portType == dest_p.getType(*this));
                    portType = dest_p.getType(*this);
                    string srcPortType = src_p.getType(*this);

                    cout << "portType = " << portType << "\n";
                    cout << "srcportName = " << src_p.longName << "\n";
                    cout << "destportName = " << dest_p.longName << "\n";

                    if (portType == "output")
                    {
                        cout << "sys_c_port_name = " << submod->get_output(src_p.PortName)->name() << "\n";
                        get_output(dest_p.PortName)->bind(*submod->get_output(src_p.PortName));
                    }
                    else if (portType == "int")
                    {
                        get_output(dest_p.PortName)->bind(*submod->get_internal(src_p.PortName));
                    }
                    else if (portType == "t_socket")
                    {
                        //if dest port of current module is a target, then there can be 
                        // two options from the sub module src port
                        // 1) SPM 2) another hierarchy target socket

                        if (SPM *spm = dynamic_cast<SPM *>(submod))
                        {
                            cout << "attempting to bind the socket SPM bla.\n";
                            assert(src_p.getType(*this) == "ts_socket");
                            // spm->get_socket(src_p.PortName)->bind(*get_t_socket(dest_p.PortName));
                            get_t_socket(dest_p.PortName)->bind(*spm->get_socket(src_p.PortName));
                            cout << "bind the socket : done.\n";
                        }
                        else
                        {
                            assert(src_p.getType(*this) == "t_socket");
                            cout << "attempting to bind the socket bla.\n";
                            // get_socket(dest_p.PortName)->bind(*submod->get_socket(src_p.PortName));
                            get_t_socket(dest_p.PortName)->bind(*submod->get_t_socket(src_p.PortName));
                            // submod->get_t_socket(src_p.PortName)->bind(*get_t_socket(dest_p.PortName));
                            cout << "bind the socket : done.\n";
                        } 
                    }
                    else if (portType == "i_socket")
                    {
                        //if dest port is a intiator of current module, then there can be 
                        // two options from the sub module source port
                        // 1) MDP 2) another hierarchy initator socket

                        if (MDP *mdp = dynamic_cast<MDP*>(submod))
                        {
                            cout << "attempting to bind the socket MDP bla.\n";
                            assert(src_p.getType(*this) == "is_socket");
                            // get_i_socket(dest_p.PortName)->bind(*mdp->get_socket(src_p.PortName));
                            mdp->get_socket(src_p.PortName)->bind(*get_i_socket(dest_p.PortName));
                            cout << "bind the socket : done.\n";
                        }
                        else
                        {
                            assert(src_p.getType(*this) == "i_socket");
                            cout << "attempting to bind the socket bla.\n";
                            // get_socket(dest_p.PortName)->bind(*submod->get_socket(src_p.PortName));
                            // get_socket(dest_p.PortName)->bind(*submod->get_socket(src_p.PortName));
                            submod->get_i_socket(src_p.PortName)->bind(*get_i_socket(dest_p.PortName));
                            cout << "bind the socket : done.\n";
                        } 
                    }
                    else
                    {
                        cout << "Port type = " << portType << "\n";
                        assert(false);
                    }
                    bounded = true;
                }

                if (bounded)
                {
                    cout << "SUBMOD::src=" << src_p.longName << ",dest=" << dest_p.longName << "\n";
                }
                else
                {
                    assert(portType != "socket");
                }

                cout << "check 3\n";
            }

            }

            for (const pair<Port, set<Port>> pair : possibleMuxes)
            {
                Port dest_p = pair.first;
                Port src_p = *pair.second.begin();
                bool bounded = false;
                string portType = "none";


                if (src_p.moduleName != "THIS" && dest_p.moduleName != "THIS")
                {
                    string srcPortType = src_p.getType(*this);
                    string destPortType = dest_p.getType(*this);

                    CMOD *srcSubMod = getSubMod(src_p.moduleName);
                    CMOD *destSubMod = getSubMod(dest_p.moduleName);

                    cout << "srcPortName = " << src_p.longName << ",type=" << srcPortType << "\n";
                    cout << "destPortName = " << dest_p.longName << ",type=" << destPortType << "\n";

                    assert(pair.second.size() == 1);

                    //Only possible type of connections are
                    // i --> t , is --> t , i --> ts , is --> ts
                    if(srcPortType == "i_socket"){
                        if (destPortType == "t_socket"){
                            srcSubMod->get_i_socket(src_p.PortName)->bind(*destSubMod->get_t_socket(dest_p.PortName));
                        }
                        else if(destPortType == "ts_socket"){
                            SPM *spm = static_cast<SPM *>(destSubMod);
                            srcSubMod->get_i_socket(src_p.PortName)->bind(*spm->get_socket(dest_p.PortName));
                        }
                        else{
                            assert(false);
                        }
                    }
                    else if(srcPortType == "is_socket"){
                        MDP *mdp = static_cast<MDP*>(srcSubMod);
                        if (destPortType == "t_socket"){
                            mdp->get_socket(src_p.PortName)->bind(*destSubMod->get_t_socket(dest_p.PortName));
                        }
                        else if(destPortType == "ts_socket"){
                             SPM *spm = static_cast<SPM *>(destSubMod);
                             mdp->get_socket(src_p.PortName)->bind(*spm->get_socket(dest_p.PortName));
                        }
                        else{
                            assert(false);
                        }
                    }
                    else{
                        assert(false);
                    }
                }
        }
    }

    SC_METHOD(route);
    sensitive << clk.pos();
    for (sc_in<Word> &i : inputs)
    {
        sensitive << i;
    }
    for (sc_out<Word> &o : outputs)
    {
        sensitive << o;
    }
    for (sc_signal<Word> &s : internals)
    {
        sensitive << s;
    }

    for (const unique_ptr<CMOD> &cp : submods)
    {
        CMOD *sm = cp.get();
        for (pair<string, int> outNamePair : sm->outputNameMap)
        {
            string outName = outNamePair.first;
            sensitive << *sm->get_output(outName);
        }
    }

    cout << "constructor done.\n";
}