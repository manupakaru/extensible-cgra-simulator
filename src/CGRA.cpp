#include "CGRA.h"

#include <nlohmann/json.hpp>

// for convenience
using json = nlohmann::json;
using namespace std;

void CGRA::parseJSON(ifstream& fileIn){
    json fileJSON;
    fileIn >> fileJSON;

    for (auto& el : fileJSON["ARCH"].items()){
        std::cout << el.key() << " : " << el.value() << "\n";
    }

}