#ifndef STIM_FU_H
#define STIM_FU_H

#include "systemc.h"
#include "type_defs.h"

SC_MODULE(stim_fu)
{
  sc_out<Word> fu_I1, fu_I2;
  sc_in<bool> clk;

  void StimGen()
  {
    fu_I1.write(Word(1));
    fu_I2.write(2);
    wait();
    fu_I1.write(2);
    fu_I2.write(3);
    wait();
    wait();
    wait();
    sc_stop();
  }
  SC_CTOR(stim_fu)
  {
    SC_THREAD(StimGen);
    sensitive << clk.pos();
  }
};



#endif
