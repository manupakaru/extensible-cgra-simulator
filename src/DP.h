#ifndef DP_H
#define DP_H

#include "systemc.h"
#include <stdio.h>
#include <set>
#include <map>
#include <string>
#include <memory>
#include <sstream>
#include "Module.h"

// Needed for the simple_target_socket
#define SC_INCLUDE_DYNAMIC_PROCESSES

using namespace sc_core;
using namespace sc_dt;
using namespace std;

#include "tlm.h"
#include "tlm_utils/simple_initiator_socket.h"

#include "type_defs.h"
#include <nlohmann/json.hpp>
using json = nlohmann::json;
using namespace std;

struct DP : public CMOD
{
protected:
public:
    void compute();
    bool parseConst(string s, int& val);
    
    DP(sc_module_name name, map<string, json> &all_mods, json &hardware_template, string type, json &prog, json &parent_prog, int _ii)
        : CMOD(name, all_mods, hardware_template, type, prog, parent_prog, _ii)
    {
        SC_HAS_PROCESS(DP);
        SC_CTHREAD(compute, clk.pos());


    }
};

#endif