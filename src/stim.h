#include "systemc.h"
SC_MODULE(stim)
{
  sc_out<bool> enable, reset;
  sc_in<bool> clk;

  void StimGen()
  {
    enable.write(true);
    reset.write(true);
    wait();
    enable.write(true);
    reset.write(false);
    wait();
    wait();
    wait();
    // sc_stop();
  }
  SC_CTOR(stim)
  {
    SC_THREAD(StimGen);
    sensitive << clk.pos();
  }
};
