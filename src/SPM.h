#ifndef SPM_H
#define SPM_H

#include "systemc.h"
#include <stdio.h>
#include <set>
#include <map>
#include <string>
#include <memory>
#include <sstream>
#include <iostream>
#include "Module.h"
#include <iomanip> 

// Needed for the simple_target_socket
#define SC_INCLUDE_DYNAMIC_PROCESSES

using namespace sc_core;
using namespace sc_dt;
using namespace std;

#include "tlm.h"
#include "tlm_utils/simple_target_socket.h"

#include <nlohmann/json.hpp>
using json = nlohmann::json;
using namespace std;


struct SPM : public CMOD{
    protected:
        // vector<uint8_t> mem;
        map<int,uint8_t> mem;


    public:
        sc_vector<tlm_utils::simple_target_socket<SPM>> ts_sockets;
        tlm_utils::simple_target_socket<SPM>* get_socket(string name);

        map<string,int> tsSocketNameMap;



        int size;
        int no_ports;

        virtual void b_transport( tlm::tlm_generic_payload& trans, sc_time& delay);

        ~SPM(){
            string pathPrefix = "../json/";
            string moduleName = this->name();
            string fileName = pathPrefix + moduleName + "_memout.json";
            std::ofstream outMemFile(fileName.c_str());
            
            json outMem;
            for(pair<int,uint8_t> p : mem){
                stringstream iss;
                iss << "0x" << std::setfill ('0') << std::setw(2) <<  hex << (int)p.second;
                outMem["data"][to_string(p.first)]=iss.str();
            }
            outMemFile << setw(4) << outMem << endl;
            outMemFile.close();
        }

        SPM(sc_module_name name, map<string, json> & all_mods, json & hardware_template, string type, json & prog, json& parent_prog, int _ii) : CMOD(name,all_mods,hardware_template,type,prog,parent_prog,_ii){
        // SPM(sc_module_name name, int size, int ports, string fileName) : sc_module(name), sockets("port"){
        // Register callback for incoming b_transport interface method call
            // SC_HAS_PROCESS(SPM);

            i_socketNameMap.clear();
            t_socketNameMap.clear();

            if (hardware_template.find("SOCKETS") != hardware_template.end())
            {
                cout << "SOCKETS size = " << hardware_template["SOCKETS"].size() << "\n";
                ts_sockets.init(hardware_template["SOCKETS"].size());
                cout << "SOCKETS:";

                int count = 0;
                for (auto &el : hardware_template["SOCKETS"].items())
                {
                cout << el.value() << ",";
                tsSocketNameMap[el.value()] = count;
                portType[el.value()] = "ts_socket";
                count++;
                }
                cout << "\n";
            }     

            int size = hardware_template["MEM"]["SIZE"];
            int ports = hardware_template["SOCKETS"].size();
            string fileName = hardware_template["INIT_FILE"];

            this->size = size;
            this->no_ports = ports;

            for(int i = 0 ; i < no_ports ; i++){
                ts_sockets[i].register_b_transport(this, &SPM::b_transport);
            }

            // Initialize memory with zeros
            // for (int i = 0; i < size; i++){
            //     mem.push_back(0);
            // }

            // Read generated
            if(!fileName.empty()){
                std::ifstream dataFile(fileName.c_str()); assert(dataFile);
                json dataJSONFile;
                dataFile >> dataJSONFile;

                cout << "init file parsed :: " << fileName << "\n";

                assert(dataJSONFile.find("data")!=dataJSONFile.end());
                for(auto& el : dataJSONFile["data"].items()){
                    int addr = -1;
                    int8_t data = -1;

                    
                    {
                        stringstream iss(el.key());
                        iss >> addr;
                        if(iss.fail()){
                            cout << "parsing addr failure.!\n";
                            assert(false);
                        }
                    }

                    {
                        string val = el.value();
                        stringstream iss(val);
                        int data_int;
                        iss >> hex >> data_int;
                        data = data_int;
                        if(iss.fail()){
                            cout << "parsing data failure.!\n";
                            assert(false);
                        }
                    }

                    if(addr >= size){
                        cout << "initial addr is out of bounds.!\n";
                        assert(false);
                    }

                    cout << "SPM_INIT : addr=" << addr << ",data=" << (int)data << ",el.value() = " << el.value() << "\n";
                    mem[addr] = data;
                }
            }
        }


};




















#endif