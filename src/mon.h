#include "systemc.h"
#include <stdio.h>

using namespace std;

SC_MODULE(mon)
{
  sc_in<bool> clk;
  sc_in<sc_uint<4>> counter;

  void StimGen()
  {
    cout << "time=" << sc_time_stamp() << ",";
    cout << "counter=" << counter << "\n";    
  }
  SC_CTOR(mon)
  {
    SC_METHOD(StimGen);
    sensitive << clk.pos();
  }
};
