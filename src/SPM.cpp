#include "SPM.h"

void SPM::b_transport( tlm::tlm_generic_payload& trans, sc_time& delay){

    // assert(false);

    tlm::tlm_command cmd = trans.get_command();
    sc_dt::uint64    adr = trans.get_address();
    unsigned char*   ptr = trans.get_data_ptr();
    unsigned int     len = trans.get_data_length();
    unsigned char*   byt = trans.get_byte_enable_ptr();
    unsigned int     wid = trans.get_streaming_width();

    // Obliged to check address range and check for unsupported features,
    //   i.e. byte enables, streaming, and bursts
    // Can ignore DMI hint and extensions
    // Using the SystemC report handler is an acceptable way of signalling an error

    if ( adr >= size  || len > 4 || wid < len || byt != 0){
      string error = "Target : " + string(this->basename()) + "," + "does not support given generic payload transaction";
      SC_REPORT_ERROR("TLM-2", error.c_str());
    }

    // if(cmd == tlm::TLM_READ_COMMAND && byt != 0){
    //   string error = "Target : " + string(this->basename()) + "," + "does not support given generic payload transaction";
    //   SC_REPORT_ERROR("TLM-2", error.c_str());
    // }

    if(cmd == tlm::TLM_READ_COMMAND){
        cout << "SPM:: reading addr=" << adr << ",data=" <<  mem[adr] <<  ",len=" << len << "\n";
        memcpy(ptr, &mem[adr], len); 
    }
    else{
        memcpy(&mem[adr], ptr, len);
        cout << "SPM:: writing addr=" << adr << ",data=" <<  mem[adr] <<  ",len=" << len << "\n";
    }


    trans.set_response_status( tlm::TLM_OK_RESPONSE );
}

tlm_utils::simple_target_socket<SPM>* SPM::get_socket(string name){
  assert(tsSocketNameMap.find(name) != tsSocketNameMap.end());
  tlm_utils::simple_target_socket<SPM>* retVal = &ts_sockets[tsSocketNameMap[name]];
  assert(retVal);
  return retVal;
}